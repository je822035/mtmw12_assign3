# MTMW12_assign3

Assignment 3. To evaluate a the geostrophic wind relation in terms of a
numerical difference formula and its exact solution.

geostrophic_calc.py defines the physical properties required to calculate the
pressure, the pressure gradient, the finite difference pressure gradient and the
geostrophic wind. This is imported and used in all subsequent files as it
contains the functions to calculate these quantities.

geo_evaluation.py implements the calculations for two-point finite difference
formulae. It also produces plots which compare the analytical and the numerical
solution.

testing_geo.py designs and explains an experiment to find out the order of
convergence for the two-point difference at the mid-points. It produces a value
for the order of convergence and plots the errors at different resolutions.

geo_three_point.py implements a three-point finite difference formula for the
end-points and plots the analaytical and the numerical solution to show how this
affects the solution.

testing_three_point.py uses a similar test as before to show the order of
convergence is higher for the three-point difference formula, just at the end
points.