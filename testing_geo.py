"""
Assignment 3 Robyn Ingram 28822035
This code contains what is required to run a code to test the numerical
solution and its order of convergence. The comments explain how to design and
implement this and produce a plot to show this property holds.
Running num_test() will execute this.
"""

import numpy as np
import matplotlib.pyplot as plt

# This file contains the calculation for the numerial pressure gradient
# pressure_formula calculates the pressure
# pressure_grad calculates the numerical solution
# pressure_grad_exact calculates the analytical solution
import geostrophic_calc as geo

# running this function will implement the test
def num_test():
    """For different values of N corresponding to different resolutions
    delta_y, calculates the error of the numerical solution versus the exact
    solution and plots this to demonstrate the order of convergence is 2"""
    
    # For the domain y we take multiple values of N say 5
    N_values = 5
    
    # Choose suitable N that will give you an equivalent mid-point to compare
    # N must be greater than 3 for the second-order formula to be used
    # I take N = 10, 20, 40, 80 and 160
    # As these will all give me a middle point of 500km
    N = np.array([10, 20, 40, 80, 160])
    
    # Defines start and end of y domain (these are defined properties)
    y_min = geo.Phys_prop['y_min']
    y_max = geo.Phys_prop['y_max']
    
    # Calculate the resolution delta_y 
    # delta_y is the size of each interval
    delta_y = (y_max - y_min) / N
    
    # For each N we do a set of calculations
    # To work out the error at a specific mid-point
    
    # Initialise array for the error at the mid=point
    err_mid = np.zeros(N_values)
    
    for i in range(N_values):
        n = N[i]
        
        # Calculate the y domain
        y = np.linspace(y_min, y_max, n + 1)
        
        # Calculate the pressure for all in y domain
        p = geo.pressure_formula(y)
        
        # Calculate the numerical solution of dpdy
        num_dpdy = geo.pressure_grad(p, delta_y[i], n)
        
        # Calculate the exact solution of dpdy
        ana_dpdy = geo.pressure_grad_exact(p, y)
        
        # Calculate the absolute error between the two solutions
        err_dpdy = abs(ana_dpdy - num_dpdy)
        
        # Pick the error at a suitable mid-point i.e. N/2 + 1
        # For this example that mid-point is 500 km
        # The array position of this will vary for each y
        n_mid = int(n/2 + 1)
        err_mid[i] = err_dpdy[n_mid]
        
    # Now we can work out a value for the order of convergence
    # First we plot our findings for all resolutions on a log-log graph
    
    # the font for graphs
    font = {'size' : 14}
    plt.rc('font', **font)
    
    # log-log plot of error against resolution
    # The gradient will be the order of convergence
    plt.clf()
    plt.loglog(delta_y, err_mid, 'k')
    plt.xlabel("log(delta_y)")
    plt.ylabel("log(error)")
    plt.grid(which = "both")
    plt.tight_layout()
    plt.savefig("Numerical_test.pdf")
    plt.show()
    
    # From our calculations we can now choose two errors
    # Corresponding to two resolutions
    # I will take the first and the last i.e. N = 10 and N = 160
    
    # For N = 10
    e1 = err_mid[0]
    d1 = delta_y[0]
    
    # For N = 160
    e2 = err_mid[-1]
    d2 = delta_y[-1]
    
    # The order of convergence is calculated using this formula
    order_n = (np.log(e1) - np.log(e2)) / (np.log(d1) - np.log(d2))
    
    # Giving us an order close to 2
    print(order_n)