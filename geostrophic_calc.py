"""
Assignment 3 Robyn Ingram 28822035
Code to calculate the geostrophic wind using defined physical properties both
analytically and using a Taylor series difference formula for the numerical
solution.
"""

import numpy as np

# Physical properties stored in a dictionary
Phys_prop = {
    'p_a' :   1e5,    # the mean pressure                     [units Pa]
    'p_b' :   200,    # the magnitude of pressure variations  [units Pa]
    'f' :     1e-4,   # coriolis parameter                    [units s**-1]
    'rho' :   1.,     # density                               [units kgm**-3]
    'L' :     2.4e+6, # lengthscale of pressure variations    [units m]
    'y_min' : 0.,     # Start of the y location               [units m]
    'y_max' : 1e6}    # End of the y location                 [units m]


def pressure_formula(y):
    """The pressure p at locations y based on given formula and Phys_prop
    defined properties"""
    
    p_a = Phys_prop['p_a']
    p_b = Phys_prop['p_b']
    L = Phys_prop['L']
    
    p = p_a + p_b*np.cos(y*np.pi / L)
    return p

def pressure_grad_exact(p, y):
    """The analytical solution for dp/dy with given pressures p at locations
    y, using the given formula for p and the physical properties defined in
    Phys_prop"""
    
    L = Phys_prop['L']
    p_b = Phys_prop['p_b']
    
    dpdy = - np.sin(y * np.pi / L) * p_b * np.pi / L
    return dpdy

def pressure_grad(p, delta_y, N):
    """The numerical differentiation for pressure gradient dp/dy using the
    centred finite difference formula at 2nd order for pressures p_j (where
    j = 0, ..., N) with distance between points delta_y"""
    
    # initialise dpdy
    dpdy = np.zeros_like(p)
    
    # At the end-points use two-point differences
    # For p_0 use the Forward Difference Formula
    dpdy[0] = (p[1] - p[0]) / delta_y
    
    # For p_N use the Backward Difference Formula
    dpdy[-1] = (p[-1] - p[-2]) / delta_y
    
    # For p_1 to p_(N-1) use the centred 2nd order difference formula
    for i in range(1, N):
        dpdy[i] = (p[i+1] - p [i-1]) / (2*delta_y)
    
    return dpdy

def wind(dpdy):
    """The wind speed u calculated from the geostrophic wind relation using
    the numerically evaluated dpdy, density rho and the coriolis parameter f"""
    
    rho = Phys_prop['rho']
    f = Phys_prop['f']
    
    # Horizontal wind speed component
    u = - dpdy / (rho * f)
    return u
    