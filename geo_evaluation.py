"""
Assignment 3 Robyn Ingram 28822035
By running Geostrophic_wind(), this calculates the exact solution of u and the
numerical solution of u using two-point difference formula and saves two plots
comparing the two solutions and comparing the error of the numerical solution.
"""

import matplotlib.pyplot as plt
import numpy as np
import geostrophic_calc as geo

def Geostrophic_wind():
    """Calculates the wind u from the pressure p by numerically differentiating
    the geostrophic wind relation for some fixed locations y. Plots the
    analytical solution against the numerical solution and an error plot."""
    
    # Locations y as an array from y_min to y_max
    # N is the number of intervals to divide the locations into
    N = 10
    y_min = geo.Phys_prop['y_min']
    y_max = geo.Phys_prop['y_max']
    y = np.linspace(y_min, y_max, N + 1)
    
    # Calculate the pressure for each location y
    p = geo.pressure_formula(y)
    
    # Numerical differentiation to find the pressure gradient dp/dy
    # delta_y is the distance between each of the fixed location points
    delta_y = (y_max - y_min) / N
    dpdy = geo.pressure_grad(p, delta_y, N)
    
    # finds corresponding u
    u = geo.wind(dpdy)
    
    # Exact solution for u
    dpdy_exact = geo.pressure_grad_exact(p, y)
    u_exact = geo.wind(dpdy_exact)
    
    # The font for graphs
    font = {'size' : 14}
    plt.rc('font', **font)
    
    # Plot the analytical solution and the numerical solution
    plt.plot(y/1000, u_exact, 'r-', label = "Analytical Solution")
    plt.plot(y/1000, u, 'k.', label = "Numerical Solution")
    plt.legend()
    plt.grid()
    plt.xlabel("distance y (km)")
    plt.ylabel("wind speed u (m/s)")
    plt.tight_layout()
    plt.savefig("Geostrophic_wind_solution.pdf")
    plt.show()
    
    # Plot the error
    plt.clf()
    plt.plot(y/1000, abs(u-u_exact), '.-k')
    plt.grid()
    plt.xlabel("y (km)")
    plt.ylabel("u error (m/s)")
    plt.tight_layout()
    plt.savefig("Geo_wind_error.pdf")
    
    
    