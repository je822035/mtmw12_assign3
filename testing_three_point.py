"""
Assignment 3 Robyn Ingram 28822035
An experiment to find the order of convergence for an end-points using both
two-point difference formula and three-point difference formula. These
findings are presented on a graph and the order of convergence found in the
function three_point_test()
"""

import numpy as np
import matplotlib.pyplot as plt

# This file contains the calculation for the numerial pressure gradient
# pressure_formula calculates the pressure
# pressure_grad calculates the numerical solution
# pressure_grad_exact calculates the analytical solution
import geostrophic_calc as geo

# This has the calculation for the three point formula
from geo_three_point import end_points

# running this function will implement the test
def three_point_test():
    """For different values of N corresponding to different resolutions
    delta_y, calculates the error of the numerical solution versus the exact
    solution at the end-points using the two-point formula and three-point
    formula and plots this to demonstrate the order of convergence."""
    
    # For the domain y we take multiple values of N say 5
    N_values = 5
    
    # Choose any N greater than 3 for the formula to be used
    # I will choose the end-point at 1000 km to illustrate this
    # I take N = 10, 20, 40, 80 and 160, same as in previous experiment
    N = np.array([10, 20, 40, 80, 160])
    
    # Defines start and end of y domain (these are defined properties)
    y_min = geo.Phys_prop['y_min']
    y_max = geo.Phys_prop['y_max']
    
    # Calculate the resolution delta_y 
    # delta_y is the size of each interval
    delta_y = (y_max - y_min) / N
    
    # For each N we do a set of calculations
    # To work out the error at a specific end-point
    
    # Initialise array for the error at the end-point
    err_2pt = np.zeros(N_values)
    err_3pt = np.zeros(N_values)
    
    for i in range(N_values):
        n = N[i]
        
        # Calculate the y domain
        y = np.linspace(y_min, y_max, n + 1)
        
        # Calculate the pressure for all in y domain
        p = geo.pressure_formula(y)
        
        # Calculate the numerical solution of dpdy with 2 point formula
        dpdy_2pt = geo.pressure_grad(p, delta_y[i], n)
        
        # Initialise three-point formula
        dpdy_3pt = dpdy_2pt.copy()
        
        # Calculate the numerical solution with 3 point formula at end-points
        dpdy_3pt = end_points(p, dpdy_3pt, delta_y[i])
        
        # Calculate the exact solution of dpdy
        ana_dpdy = geo.pressure_grad_exact(p, y)
        
        # Pick the error at one end-point say 1000 km (i.e. the last term)
        # Calculate the absolute error between the numerical and exact solutio
        err_2pt[i] = abs(ana_dpdy[-1] - dpdy_2pt[-1])
        err_3pt[i] = abs(ana_dpdy[-1] - dpdy_3pt[-1])
        
    # Now we can work out a value for the order of convergence
    # First we plot our findings for all resolutions on a log-log graph
    
    # the font for graphs
    font = {'size' : 14}
    plt.rc('font', **font)
    
    # log-log plot of error against resolution
    # The gradient will be the order of convergence
    plt.clf()
    plt.loglog(delta_y, err_2pt, '--k', label = "Two-point")
    plt.loglog(delta_y, err_3pt, 'k', label = "Three-point")
    plt.legend(loc = "lower right")
    plt.xlabel("log(delta_y)")
    plt.ylabel("log(error)")
    plt.grid(which = "both")
    plt.tight_layout()
    plt.savefig("2pt_test.pdf")
    plt.show()
    
    # From our calculations we can now choose errors
    # Corresponding to two resolutions for the 2-point and 3-point approach
    # I will take the first and the last i.e. N = 10 and N = 160
    
    # For N = 10 (e1 is 2pt error and f1 is 3pt error)
    e1 = err_2pt[0]
    f1 = err_3pt[0]
    d1 = delta_y[0]
    
    # For N = 160
    e2 = err_2pt[-1]
    f2 = err_3pt[-1]
    d2 = delta_y[-1]
    
    # The order of convergence is calculated using this formula
    order_2pt = (np.log(e1) - np.log(e2)) / (np.log(d1) - np.log(d2))
    order_3pt = (np.log(f1) - np.log(f2)) / (np.log(d1) - np.log(d2))
    
    # Hence the two-point is roughly first order
    # The three-point is roughly second order
    print(order_2pt)
    print(order_3pt)