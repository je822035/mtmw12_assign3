"""
Assignment 3 Robyn Ingram 28822035
Implement a more accurate numerical solution for the geostrophic wind relation.
The method I'm using is to extend the end-points to a three-point difference
formula which will make the results second-order accurate. I use the same
evaluation as in Question 1 but with some adjustments.
Running New_geo_wind() will implement these changes
"""
import matplotlib.pyplot as plt
import numpy as np
import geostrophic_calc as geo

# Develop a function that will implement the three-point difference
def end_points(p, dpdy, delta_y):
    """Calculates the numerical solution of dpdy for the end-points at p_0 and
    p_N with intervals of delta_y using the pressure p. This is calculated
    using a one-sided, three-point, second-order numerical difference
    formula. This replaces dpdy calculated from the two-point method."""
    
    # For p'0 we consider p0, p1 and p2 and use the method
    dpdy[0] = (4*p[1] - p[2] - 3*p[0]) / (2*delta_y)
    
    # For p'N we consider pN, pN-1 and pN-2
    dpdy[-1] = (3*p[-1] + p[-3] - 4*p[-2]) / (2*delta_y)
    
    return dpdy

# Using the previous system, plot graphs
def New_geo_wind():
    """Calculates the wind u from the pressure p by numerically differentiating
    the geostrophic wind relation for some fixed locations y. Plots the
    analytical solution against the numerical solution and an error plot."""
    
    # Locations y as an array from y_min to y_max
    # N is the number of intervals to divide the locations into
    N = 10
    y_min = geo.Phys_prop['y_min']
    y_max = geo.Phys_prop['y_max']
    y = np.linspace(y_min, y_max, N + 1)
    
    # Calculate the pressure for each location y
    p = geo.pressure_formula(y)
    
    # Numerical differentiation to find the pressure gradient dp/dy
    # delta_y is the distance between each of the fixed location points
    delta_y = (y_max - y_min) / N
    dpdy = geo.pressure_grad(p, delta_y, N)
    print(dpdy)
    
    # Implement second-order accuracy for the end-points
    # Replacing previous dpdy for these points
    dpdy = end_points(p, dpdy, delta_y)
    print(dpdy)
    
    # finds corresponding u
    u = geo.wind(dpdy)
    
    # Exact solution for u
    dpdy_exact = geo.pressure_grad_exact(p, y)
    u_exact = geo.wind(dpdy_exact)
    
    # The font for graphs
    font = {'size' : 14}
    plt.rc('font', **font)
    
    # Plot the analytical solution and the numerical solution
    plt.plot(y/1000, u_exact, 'r-', label = "Analytical Solution")
    plt.plot(y/1000, u, 'k.', label = "Numerical Solution")
    plt.legend()
    plt.grid()
    plt.xlabel("distance y (km)")
    plt.ylabel("wind speed u (m/s)")
    plt.tight_layout()
    plt.savefig("new_numerical.pdf")
    plt.show()
    
    # Plot the error
    plt.clf()
    plt.plot(y/1000, abs(u-u_exact), '.-k')
    plt.grid()
    plt.xlabel("y (km)")
    plt.ylabel("u error (m/s)")
    plt.tight_layout()
    plt.savefig("new_error.pdf")

    